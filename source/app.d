import w4d;
import w4d : Task;

import widget.root;

import vibe.core.core;
import vibe.core.core : VibeTask = Task;
import vibe.core.concurrency;

import configuration;
import download;

struct AppShutdown
{
}

int main(string[] args)
{
	auto app = new App(args);

	auto waiter = runTask({ receiveOnly!AppShutdown; });
	runTask(&downloadGameFiles);

	auto win = new Window(vec2i(550, 600), "TTR Launcher");
	app.addTask(win);
	app.addTask(new VibeEventTask(win, waiter));

	win.setContent(new LauncherRoot(win));
	win.show();

	return app.exec();
}

class VibeEventTask : Task
{
	Window _window;
	VibeTask _task;

	this(Window window, VibeTask task)
	{
		_window = window;
		_task = task;
	}

	bool exec(App app)
	{
		if (!processEvents)
		{
			_window.close();
			return true;
		}

		if (!_window.alive)
		{
			_task.send(AppShutdown());
			exitEventLoop(true);
			return true;
		}

		return false;
	}
}
