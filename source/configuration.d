module configuration;

import standardpaths;
import std.path;

import vibe.core.file;
import vibe.core.log;
import vibe.data.json;

struct Config
{
@optional:
	string gamePath = "~/.toontown-rewritten";
	string thumbnail = "https://cdn.discordapp.com/icons/300386182682902529/3488b2f7d4406cdc84bb8db5a23cc545.png?size=2048";
}

__gshared Config config;

shared static this()
{
	string[] configDirs = standardPaths(StandardPath.config, "ttr-launcher-lite");
	foreach (dir; configDirs)
	{
		auto configFile = buildPath(dir, "config.json");
		logDiagnostic("Looking for configurations in %s", configFile);
		if (existsFile(configFile))
		{
			auto json = parseJsonString(readFileUTF8(configFile), configFile);
			config = deserializeJson!Config(json);
			logDiagnostic("Loaded config from %s", configFile);
			return;
		}
	}
	logDiagnostic("No config found, loading default");
}
