module api.ttr;

import vibe.data.json;
import vibe.data.serialization;
import vibe.web.rest;

struct LoginResponse
{
	enum State : string
	{
		false_ = "false",
		partial = "partial",
		true_ = "true",
		delayed = "delayed"
	}

	State success;
@optional:
	// status = false, partial
	string banner;

	// status = partial
	string responseToken;

	// status = true
	string gameserver;
	string cookie;

	// status = delayed
	string eta;
	string position;
	string queueToken;
}

@path("/api/")
interface LoginAPI
{
@queryParam("format", "format") :
	@path("/login")
	LoginResponse postLogin(string username, string password, string format = "json");

	@path("/login")
	LoginResponse postToken(string appToken, string authToken, string format = "json");

	@path("/login")
	LoginResponse postQueue(string queueToken, string format = "json");
}
