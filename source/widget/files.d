module widget.files;

import w4d;
import w4d.style.widget;

import widget.root;

import download;

import std.conv;
import std.format;
import std.uni;

class FilesWidget : PanelWidget
{
	this()
	{
		super();
		setLayout!VerticalLineupLayout;
	}

	void update(FileDownloadTask[] newTasks)
	{
		while (newTasks.length > _children.length)
			addChild(new FileInfoWidget());
		if (newTasks.length < _children.length)
			_children = _children[0 .. newTasks.length];

		foreach (i, child; _children)
			(cast(FileInfoWidget)child).update(newTasks[i]);
	}
}

class FileInfoWidget : PanelWidget
{
	TextWidget statusLabel, nameLabel, extraLabel;
	TextWidget progressWidget;

	this()
	{
		super();
		setLayout!VerticalMonospacedSplitLayout;

		style.box.size.width = 100.percent;

		style.box.paddings = Rect(16.pixel);
		style.box.size.height = 80.pixel;
		style.box.borderWidth.bottom = 1.pixel;
		style.colorsets[WidgetState.None]["border"] = vec4(1, 1, 1, 1);

		auto horizontal = new PanelWidget();
		horizontal.setLayout!HorizontalLineupLayout;
		horizontal.addChild(statusLabel = label("[Queued] \u200b"d));
		horizontal.addChild(nameLabel = label(""d));
		addChild(horizontal);
		addChild(extraLabel = label(""d));
		addChild(progressWidget = label("0 %"d));
	}

	FileDownloadTask oldTask;

	void update(FileDownloadTask task)
	{
		if (oldTask.status != task.status)
			statusLabel.loadText("["d ~ task.status.to!dstring.camelCase ~ "] \u200b"d);

		if (oldTask.extra != task.extra)
			extraLabel.loadText(task.extra);

		if (oldTask.progress != task.progress)
			progressWidget.loadText(format!"%.2f %%"d(task.progress * 100));

		if (oldTask.file != task.file)
			nameLabel.loadText(task.file.to!dstring);

		oldTask = task;
	}
}

dstring camelCase(dstring s)
{
	dstring ret;
	bool wasUpper = true;
	foreach (i, c; s)
	{
		if (wasUpper)
			ret ~= (i == 0 ? c.toUpper : c);
		else
		{
			if (c.isUpper)
				ret ~= ' ';
			ret ~= c;
		}
		wasUpper = c.isUpper;
	}
	return ret;
}
