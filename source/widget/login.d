module widget.login;

import w4d.event;
import w4d;

import widget.root;

class LoginWidget : PanelWidget
{
	LineInputWidget username, password;
	CheckBoxWidget savePassword;
	ButtonWidget button;
	EventHandler!(void, LoginWidget, dstring, dstring, bool) onLogin;

	this()
	{
		super();
		setLayout!TableLayout(2, 3);

		style.box.size.height = 100.pixel;
		style.box.margins.top = 25.pixel;
		style.box.margins.right = 16.pixel;
		style.box.margins.bottom = 25.pixel;

		addChild(label("Username:", vec2(1, 0.3)));
		username = new LineInputWidget;
		username.loadText(""d, fontFace);
		addChild(username);
		addChild(label("Password:", vec2(1, 0.3)));
		password = new LineInputWidget;
		password.loadText(""d, fontFace);
		password.changePasswordChar('*');
		addChild(password);
		savePassword = new CheckBoxWidget;
		savePassword.loadText("Remember"d, fontFace);
		addChild(savePassword);
		button = new ButtonWidget();
		button.style.box.size.width = username.style.box.size.width;
		button.loadText("Login"d, fontFace);
		button.onButtonPress = &clickLogin;
		addChild(button);
	}

	void clickLogin()
	{
		onLogin.call(this, username.text, password.text, savePassword.checked);
	}
}
