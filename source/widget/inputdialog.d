module widget.inputdialog;

import w4d;

class PopupInputDialogWidget : PopupTextDialogWidget
{
	LineInputWidget _input;

	this()
	{
		super();
		setLayout!VerticalLineupLayout;

		_input = new LineInputWidget();
		_children = _children[0 .. $ - 1] ~ _input ~ children[$ - 1];
	}

	void loadValue(dstring v, FontFace face = null)
	{
		_input.loadText(v, face);
	}

	dstring value() @property
	{
		return _input.text;
	}
}
