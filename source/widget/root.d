module widget.root;

import loadfont;
import w4d;
import g4d.util.bitmap;
import asyncimg;

import core.time;

import std.algorithm;
import std.conv;
import std.path;
import std.stdio : File;
import std.string;
import std.traits;

import widget.files;
import widget.login;
import widget.inputdialog;

import vibe.core.core;
import vibe.core.file;
import vibe.core.log;
import vibe.web.rest;

import api.ttr;

import configuration;
import download;

Font font;
FontFace fontFace;

TextWidget label(String)(String text) if (isSomeString!String)
{
	auto label = new TextWidget();
	label.loadText(text.to!dstring, fontFace);
	return label;
}

TextWidget label(String)(String text, vec2 gravity) if (isSomeString!String)
{
	auto label = new TextWidget();
	label.setLayout!GravityLayout(gravity);
	label.loadText(text.to!dstring, fontFace);
	return label;
}

class LauncherRoot : RootWidget
{
	this(Window window)
	{
		super();
		setLayout!VerticalSplitLayout;
		auto launcher = new TTRLauncher(window);
		launcher.style.box.size.height = 200.pixel;
		addChild(launcher);

		auto scroll = new VerticalScrollPanelWidget();
		FilesWidget files = new FilesWidget();
		scroll.contents.addChild(files);
		addChild(scroll);

		runTask({
			while (true)
			{
				files.update(fileDownloadTasks);
				sleep(50.msecs);
			}
		});
	}
}

class TTRLauncher : PanelWidget
{
	TextWidget errorLine;
	RestInterfaceClient!LoginAPI api;
	Window window;

	this(Window window)
	{
		super();
		setLayout!HorizontalSplitLayout;
		this.window = window;

		api = new RestInterfaceClient!LoginAPI("https://www.toontownrewritten.com/");

		auto fontSrc = fontFamilyByName("Roboto");
		if (fontSrc is null)
			fontSrc = fontFamilyByName("Calibri");
		if (fontSrc is null)
			fontSrc = fontFamilyByName("Arial");
		if (fontSrc is null)
			fontSrc = fontFamilyByName("OpenSans");
		if (fontSrc is null)
			fontSrc = fontFamilyByName("sans-serif");
		if (fontSrc is null)
			fontSrc = fontFamilyByName("monospace");
		if (fontSrc is null)
			throw new Exception("Unable to find fonts");

		if (auto f = "Regular" in fontSrc)
		{
			logDiagnostic("Loading Regular font from %s", *f);
			font = new Font(*f);
		}
		else
		{
			logDiagnostic("Loading %s font from %s", fontSrc.byKey.front, fontSrc.byValue.front);
			font = new Font(fontSrc.byValue.front);
		}
		fontFace = new FontFace(font, vec2i(16, 16));

		ImageWidget left = new ImageWidget();
		left.setImage(new BitmapRGBA(vec2i(1, 1), [0, 0, 0, 0]), false);
		left.style.box.margins = Rect(16.pixel);
		addChild(left);
		left.setImageAsync(config.thumbnail);

		auto p = new PanelWidget();
		auto login = new LoginWidget();
		p.addChild(login);
		runTask({ restoreSession(login); });
		login.onLogin = &this.login;
		p.addChild(errorLine = label(""d));
		addChild(p);
	}

	void login(LoginWidget login, dstring username, dstring password, bool savePassword)
	{
		if (!fileDownloadTasks.all!"a.success")
		{
			auto diag = new PopupTextDialogWidget();
			diag.loadText("Please wait until all files are downloaded."d, fontFace);
			diag.setButtons([DialogButton.Ok], fontFace);
			_context.setPopup(diag);
			return;
		}

		runTask({
			try
			{
				auto f = openFile(".cred", FileMode.createTrunc);
				scope (exit)
					f.close();
				f.write(username.to!string);
				if (savePassword)
				{
					f.write("\n");
					f.write(password.to!string);
				}
			}
			catch (Exception e)
			{
				logError("Failed to open file for credential writing: %s", e);
			}
		});

		runTask(() {
			processAPIResponse(api.postLogin(username.to!string, password.to!string));
		});
	}

	void processAPIResponse(LoginResponse res)
	{
		if (res.banner.length)
			errorLine.loadText(res.banner.to!dstring);
		switch (res.success)
		{
		case LoginResponse.State.true_:
			launchGame(res.gameserver, res.cookie);
			break;
		case LoginResponse.State.false_:
			auto diag = new PopupTextDialogWidget();
			diag.loadText("Failed to authenticate."d, fontFace);
			diag.setButtons([DialogButton.Ok], fontFace);
			_context.setPopup(diag);
			break;
		case LoginResponse.State.partial:
			auto diag = new PopupInputDialogWidget();
			diag.loadText("Authenticator Token:"d, fontFace);
			diag.loadValue(""d, fontFace);
			diag.setButtons([DialogButton.Ok, DialogButton.Cancel], fontFace);
			diag.onResult = (DialogButton button) {
				if (button == DialogButton.Ok)
					send2Factor(res.responseToken, diag.value.to!string);
			};
			_context.setPopup(diag);
			break;
		case LoginResponse.State.delayed:
			reauth(res.eta.to!int, res.queueToken,
					res.position.to!dstring);
			break;
		default:
			errorLine.loadText("Unimplemented success code: "d ~ res.success.to!dstring);
			break;
		}
	}

	void send2Factor(string responseToken, string authToken)
	{
		runTask({ processAPIResponse(api.postToken(responseToken, authToken)); });
	}

	void reauth(int after, string queueToken, dstring position)
	{
		runTask({
			while (true)
			{
				while (after > 0)
				{
					errorLine.loadText(position ~ " in queue... Retry in "d ~ after.to!dstring);
					sleep(1.seconds);
					after--;
				}
				LoginResponse ret = api.postQueue(queueToken);
				if (ret.success == LoginResponse.State.delayed)
				{
					after = ret.eta.to!int;
					position = ret.position.to!dstring;
				}
				else
				{
					processAPIResponse(ret);
					return;
				}
			}
		});
	}

	void launchGame(string gameserver, string cookie)
	{
		import std.process : environment, spawnProcess, Config;

		string dir = config.gamePath.expandTilde;

		auto env = environment.toAA;
		env["TTR_GAMESERVER"] = gameserver;
		env["TTR_PLAYCOOKIE"] = cookie;

		string exe;
		version (Windows)
			exe = buildPath(dir, "TTREngine.exe");
		else
			exe = buildPath(dir, "TTREngine");

		if (!existsFile(exe))
		{
			auto diag = new PopupTextDialogWidget();
			diag.loadText("Could not find TTREngine path."d, fontFace);
			diag.setButtons([DialogButton.Ok], fontFace);
			_context.setPopup(diag);
			return;
		}

		version (Posix)
		{
			import core.sys.posix.sys.stat : S_IXUSR, S_IXGRP, S_IXOTH;
			import std.file : getAttributes, setAttributes;

			setAttributes(exe, getAttributes(exe) | S_IXUSR | S_IXGRP | S_IXOTH);
		}

		spawnProcess([exe], nullFile("r"), nullFile("w"), nullFile("w"), env,
				Config.detached | Config.suppressConsole, dir);

		window.close();
	}

	void restoreSession(LoginWidget login)
	{
		try
		{
			if (existsFile(".cred"))
			{
				auto f = readFileUTF8(".cred").lineSplitter;
				logDiagnostic("Restoring credentials");
				login.username.loadText(f.front.to!dstring);
				f.popFront;
				if (!f.empty)
				{
					login.savePassword.setChecked(f.front.length > 0);
					login.password.loadText(f.front.to!dstring);
				}
			}
		}
		catch (Exception e)
		{
			logError("Failed to open file for credential reading: %s", e);
		}
	}
}

File nullFile(string mode)
{
	version (Posix)
		return File("/dev/null", mode);
	else
		return File("NUL", mode);
}
