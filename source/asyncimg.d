module asyncimg;

import w4d;
import g4d.util.bitmap;
import imageformats;

import std.conv;

import vibe.core.core;
import vibe.core.file;
import vibe.core.log;
import vibe.stream.operations;
import vibe.http.client;

void setImageAsync(ImageWidget img, string url)
{
	img.setImageAsync(URL(url));
}

void setImageAsync(ImageWidget img, URL url)
{
	runTask({
		try
		{
			if (url.schema == "file")
			{
				auto data = readFile(url.pathString);
				img.setImageBitmap(read_image_from_mem(data));
			}
			else if (url.schema == "http" || url.schema == "https")
			{
				ubyte[] data;
				requestHTTP(url, null, (scope res) { data = res.bodyReader.readAll; });
				img.setImageBitmap(read_image_from_mem(data));
			}
			else
				throw new Exception("Can't process URI schema '" ~ url.schema ~ "'");
		}
		catch (Exception e)
		{
			logError("Failed to load image %s: %s", url, e);
		}
	});
}

void setImageBitmap(ImageWidget img, IFImage data)
{
	if (data.c == ColFmt.RGBA)
		img.setImage(new BitmapRGBA(vec2i(data.w, data.h), data.pixels));
	else if (data.c == ColFmt.RGB)
		img.setImage(new BitmapRGB(vec2i(data.w, data.h), data.pixels));
	else
		throw new Exception("Can't convert " ~ data.c.to!string ~ " to ImageWidget bitmap");
}
