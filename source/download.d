module download;

import configuration;

import eventcore.driver;

import vibe.core.core;
import vibe.core.file;
import vibe.core.log;
import vibe.data.json;
import vibe.data.serialization;
import vibe.http.client;
import vibe.stream.operations;
import vibe.internal.interfaceproxy : asInterface;

import core.time;

import std.algorithm;
import std.conv;
import std.exception;
import std.functional;
import std.path;
import std.random;
import std.range;
import std.string;
import std.uni;

struct FileDownloadTask
{
	enum Status
	{
		queued,
		hashCheck,
		upToDate,
		downloading,
		extracting,
		finished,
		failed
	}

	Status _status;
	dstring extra;
	float progress = 0;
	string file;
	RemoteFile info;

	bool success() @property const
	{
		return _status == Status.upToDate || _status == Status.finished;
	}

	bool failed() @property const
	{
		return _status == Status.failed;
	}

	Status status() @property const
	{
		return _status;
	}

	Status status(Status value) @property
	{
		logDiagnostic("Setting %s status to %s", file, value);
		extra = "";
		progress = 0;
		return _status = value;
	}
}

FileDownloadTask[] fileDownloadTasks;
string[string] localHashes;

void downloadGameFiles()
{
	auto dir = config.gamePath.expandTilde();
	string[] mirrors;

	requestHTTP("https://www.toontownrewritten.com/api/mirrors", null, (scope res) {
		if (res.statusCode != 200)
		{
			logError("Couldn't determine mirrors for gamefiles: %s %s",
				res.statusCode, res.bodyReader.readAllUTF8);
			return;
		}
		mirrors = res.readJson().deserializeJson!(string[]);
	});

	RemoteFile[string] patchFiles;

	requestHTTP("https://cdn.toontownrewritten.com/content/patchmanifest.txt", null, (scope res) {
		if (res.statusCode != 200)
		{
			logError("Couldn't download game files: %s %s", res.statusCode, res.bodyReader.readAllUTF8);
			return;
		}
		patchFiles = res.readJson().deserializeJson!(RemoteFile[string]);
	});

	try
	{
		auto hashFile = buildPath(dir, ".hashes");
		if (existsFile(hashFile))
			localHashes = readFileUTF8(hashFile).parseJsonString.deserializeJson!(string[string]);
	}
	catch (Exception e)
	{
		logError("Failed to parse hashfile: %s", e);
	}

	version (linux)
		string platform = "linux";
	else version (OSX)
		string platform = "darwin";
	else version (Win32)
		string platform = "win32";
	else version (Win64)
		string platform = "win64";
	else
	{
		pragma(msg, "Warning: Can't download files on this platform");
		string platform = "none";
	}

	foreach (name, file; patchFiles)
	{
		if (!file.only.canFind(platform))
			continue;
		fileDownloadTasks ~= FileDownloadTask(FileDownloadTask.Status.queued, null, 0, name, file);
	}

	foreach (i, ref info; fileDownloadTasks)
		downloadPatches(dir, mirrors, info, i == fileDownloadTasks.length - 1); // @suppress(dscanner.suspicious.length_subtraction)
	while (fileDownloadTasks.any!"a.failed")
		foreach (i, ref info; fileDownloadTasks)
			downloadPatches(dir, mirrors, info, true);
}

void putLocalHash(string dir, string file, string hash)
{
	localHashes[file] = hash;
	writeFileUTF8(NativePath(buildPath(dir, ".hashes")), serializeToJsonString(localHashes));
}

struct RemoteFile
{
	struct Patch
	{
	@optional:
		string filename;
		string patchHash, compPatchHash;
	}

@optional:
	string dl;
	string[] only;
	string hash, compHash;
	Patch[string] patches;
}

void downloadPatches(string dir, string[] mirrors, ref FileDownloadTask task, bool sync)
{
	string name = task.file;
	auto file = task.info;
	string target = buildPath(dir, name);
	if (existsFile(target))
	{
		if (auto hash = name in localHashes)
			if (*hash == file.hash)
			{
				task.status = FileDownloadTask.Status.upToDate;
				task.progress = 1;
				return;
			}
		// TODO: implement patching
		goto ManualDownload;
	}
	else
	{
		ManualDownload: foreach (mirror; mirrors)
		{
			try
			{
				task.status = FileDownloadTask.Status.downloading;
				task.extra = "from " ~ URL(mirror).host.to!dstring;
				string compressed = buildPath(dir, file.dl);
				scope (failure)
					if (existsFile(compressed))
						removeFile(compressed);
				downloadFile(URL(mirror ~ file.dl), (scope input, size_t size) {
					auto fil = openFile(compressed, FileMode.createTrunc);
					scope (exit)
						fil.close();
					if (size <= 0 || input.leastSize < 0)
						fil.write(input);
					else
					{
						size_t read = 0;
						while (read < size)
						{
							task.progress = read / cast(float) size;
							auto len = min(1024 * 32, size - read);
							fil.write(input, len);
							read += len;
						}
					}
				});
				if (sync)
					extractFile(dir, compressed, target, task);
				else
					runTask(&extractFileUnsafe, dir, compressed, target, &task);
				return;
			}
			catch (Exception e)
			{
				logError("Failed downloading file %s from mirror %s: %s", name, mirror, e);
			}
		}
		task.status = FileDownloadTask.Status.failed;
	}
}

void extractFileUnsafe(string dir, string compressed, string target, FileDownloadTask* task)
{
	extractFile(dir, compressed, target, *task);
}

void extractFile(string dir, string compressed, string target, ref FileDownloadTask task)
{
	scope (failure)
		task.status = FileDownloadTask.Status.failed;
	scope (exit)
		if (existsFile(compressed))
			removeFile(compressed);
	string name = task.file;
	auto file = task.info;
	task.status = FileDownloadTask.Status.hashCheck;
	sha1compare(compressed, file.compHash, task);
	scope (failure)
		if (existsFile(target))
			removeFile(target);
	task.status = FileDownloadTask.Status.extracting;
	decompressFile(compressed, target);
	task.status = FileDownloadTask.Status.hashCheck;
	sha1compare(target, file.hash, task);
	putLocalHash(dir, name, file.hash);
	task.status = FileDownloadTask.Status.finished;
	task.progress = 1;
}

void decompressFile(string path, string dest)
{
	import std.process : spawnProcess, tryWait;
	import std.stdio : stdin, File;

	auto proc = spawnProcess(["bzip2", "-dc", path], stdin, File(dest, "wb"));
	while (!proc.tryWait.terminated)
		sleep(10.msecs);
}

ubyte[] compressBuffer = new ubyte[1024 * 1024];
void sha1compare(string path, string hash, ref FileDownloadTask task)
{
	import std.digest.sha : SHA1;
	import std.digest : toHexString;

	SHA1 sha;
	sha.start();
	auto file = openFile(path, FileMode.read);
	scope (exit)
		file.close();
	size_t total = file.size;
	while (file.dataAvailableForRead)
	{
		task.progress = 1 - (file.leastSize / cast(float) total);
		auto length = file.read(compressBuffer[0 .. min($, file.leastSize)], IOMode.all);
		if (length == 0)
			break;
		sha.put(compressBuffer[0 .. length]);
	}
	auto calc = sha.finish();
	if (!toHexString(calc)[].asLowerCase.equal(hash))
		throw new Exception("Hash mismatch in file " ~ path ~ " with hash " ~ hash);
}

// modified file from vibe.inet.urltransfer
void downloadFile(HTTPClient_ = void*)(URL url, scope void delegate(scope InputStream,
		size_t size) callback, HTTPClient_ client_ = null)
{
	import vibe.http.client;

	assert(url.username.length == 0 && url.password.length == 0, "Auth not supported yet.");
	assert(url.schema == "http" || url.schema == "https", "Only http(s):// supported for now.");

	HTTPClient client;
	static if (is(HTTPClient_ == HTTPClient))
		client = client_;
	if (!client)
		client = new HTTPClient();
	scope (exit)
	{
		if (client_ is null) // disconnect default client
			client.disconnect();
	}

	if (!url.port)
		url.port = url.defaultPort;

	foreach (i; 0 .. 10)
	{
		client.connect(url.host, url.port, url.schema == "https");
		logTrace("connect to %s", url.host);
		bool done = false;
		client.request((scope HTTPClientRequest req) {
			req.requestURL = url.localURI;
			logTrace("REQUESTING %s!", req.requestURL);
		}, (scope HTTPClientResponse res) {
			logTrace("GOT ANSWER!");

			switch (res.statusCode)
			{
			default:
				throw new HTTPStatusException(res.statusCode,
					"Server responded with " ~ httpStatusText(res.statusCode) ~ " for " ~ url.toString());
			case HTTPStatus.OK:
				done = true;
				callback(res.bodyReader.asInterface!InputStream,
					res.headers.get("Content-Length", "0").to!size_t);
				break;
			case HTTPStatus.movedPermanently:
			case HTTPStatus.found:
			case HTTPStatus.seeOther:
			case HTTPStatus.temporaryRedirect:
				logTrace("Status code: %s", res.statusCode);
				auto pv = "Location" in res.headers;
				enforce(pv !is null,
					"Server responded with redirect but did not specify the redirect location for " ~ url.toString());
				logDebug("Redirect to '%s'", *pv);
				if (startsWith((*pv), "http:") || startsWith((*pv), "https:"))
				{
					logTrace("parsing %s", *pv);
					auto nurl = URL(*pv);
					if (!nurl.port)
						nurl.port = nurl.defaultPort;
					if (url.host != nurl.host || url.schema != nurl.schema || url.port != nurl.port)
						client.disconnect();
					url = nurl;
				}
				else
					url.localURI = *pv;
				break;
			}
		});
		if (done)
			return;
	}
	enforce(false, "Too many redirects!");
	assert(false);
}
